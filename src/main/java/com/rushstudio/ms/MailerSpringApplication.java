package com.rushstudio.ms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MailerSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(MailerSpringApplication.class, args);
    }

}
