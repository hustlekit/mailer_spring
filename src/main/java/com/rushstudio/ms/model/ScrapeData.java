package com.rushstudio.ms.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class ScrapeData {

    private String filePath;
    private String competitorOne;
    private String competitorTwo;
    private String competitorThree;
}
