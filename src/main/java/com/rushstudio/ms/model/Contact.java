package com.rushstudio.ms.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Date;

@Data
@EqualsAndHashCode
@Entity
@Table(name = "contact")
@SequenceGenerator(name = "id_contact", sequenceName = "contact_seq", allocationSize = 1)
public class Contact {

    @Id
    @GeneratedValue(generator = "contact_seq", strategy = GenerationType.SEQUENCE)
    private Long id;
    private String companyName;
    private String address;
    private String city;
    private String region;
    private String zip;
    private String country;
    private String phone;
    private String email;
    private String website;
    private String category;
    private String competitorOne;
    private String competitorTwo;
    private String competitorThree;
    private boolean isNew = true;
    private Integer automationStep = 0;
    private boolean answered = false;
    private boolean active = true;
    private Date added = new Date();
    private Date lastSendDate = null;
}
