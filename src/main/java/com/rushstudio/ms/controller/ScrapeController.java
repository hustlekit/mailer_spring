package com.rushstudio.ms.controller;

import com.rushstudio.ms.model.Contact;
import com.rushstudio.ms.model.ScrapeData;
import com.rushstudio.ms.service.ContactService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import java.io.IOException;
import java.util.List;


@RestController
@RequestMapping(path = "/scrape")
public class ScrapeController {

    private ContactService contactService;

    public ScrapeController(ContactService contactService) {
        this.contactService = contactService;
    }

    @PostMapping
    @Consumes(value = "application/json")
    @Produces(value = "application/json")
    public ResponseEntity<List<Contact>> scrapeData(@RequestBody ScrapeData data) throws IOException {
        ResponseEntity<List<Contact>> result = new ResponseEntity<>(contactService.readCsv(data), HttpStatus.OK);
        return result;
    }
}
