package com.rushstudio.ms.service;

import com.rushstudio.ms.model.Contact;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class EmailService {

    private final Long dayLongValue = 1000L * 60L * 60L * 24L;

    private JavaMailSender emailSender;
    private ContactService contactService;

    public EmailService(JavaMailSender emailSender, ContactService contactService) {
        this.emailSender = emailSender;
        this.contactService = contactService;
    }

    public void sendTestMessage() {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("chris@dbagency.online");
        message.setTo("christoferrush@gmail.com");
        message.setSubject("Subject");
        message.setText("test message to jest text");
        System.out.println(message);
        emailSender.send(message);
    }

    public List<Contact> sendFirstEmail() {
        List<Contact> result = contactService.findActiveByAutomationStep(0);

        for (Contact contact : result) {
            SimpleMailMessage message = new SimpleMailMessage();
            message.setFrom("chris@dbagency.online");
            message.setTo(contact.getEmail());
            message.setSubject("Quick question about " + contact.getCompanyName());

            String text = new String(
                    "Hey," + System.lineSeparator()
                            + "I came across your website when searching for " + contact.getCategory() + " in " + contact.getCity()
                            + " and noticed that you don't show in the maps section and are not ranked in the organic listings."
                            + System.lineSeparator() + System.lineSeparator()
                            + "That's a lot of calls you're missing out on..."
                            + System.lineSeparator() + System.lineSeparator()
                            + "So I decided to make a video breaking down why your top 3 competitors are out-ranking you."
                            + System.lineSeparator() + System.lineSeparator()
                            + "These are the top 3 websites that are out-ranking you:"
                            + System.lineSeparator()
                            + contact.getCompetitorOne()
                            + System.lineSeparator()
                            + contact.getCompetitorTwo()
                            + System.lineSeparator()
                            + contact.getCompetitorThree()
                            + System.lineSeparator() + System.lineSeparator()
                            + "Are you the right person to send this video to?"
                            + System.lineSeparator() + System.lineSeparator()
                            + "If you are, please reply to this email and I'll send it to you, otherwise just let me know."
                            + System.lineSeparator() + System.lineSeparator()
                            + "Have a great day!"
                            + System.lineSeparator() + System.lineSeparator()
                            + "Chris" + System.lineSeparator()
                            + "chris@dbagency.online"
            );

            message.setText(text);
            emailSender.send(message);

            contact.setLastSendDate(new Date());
            contact.setAutomationStep(1);

            contactService.update(contact);
        }
        return result;
    }

    public List<Contact> sendSecondEmail() {
        List<Contact> allContactsToSecondEmail = contactService.findActiveByAutomationStep(1);
        List<Contact> result = new ArrayList<>();

        for (Contact contact : allContactsToSecondEmail) {

            if (diffDate(contact) > 1.5 * dayLongValue) {

                SimpleMailMessage message = new SimpleMailMessage();
                message.setFrom("chris@dbagency.online");
                message.setTo(contact.getEmail());
                message.setSubject("Quick question about " + contact.getCompanyName());

                String text = new String(
                        "Hey," + System.lineSeparator() + System.lineSeparator()
                                + "Not sure if you had the chance to read my last email, so here's a quick recap of what it said:"
                                + System.lineSeparator() + System.lineSeparator()
                                + "I made a quick video explaining why your competitors are out-ranking you in Google."
                                + System.lineSeparator() + System.lineSeparator()
                                + "These are the top 3 websites that are out-ranking you:"
                                + System.lineSeparator()
                                + contact.getCompetitorOne()
                                + System.lineSeparator()
                                + contact.getCompetitorTwo()
                                + System.lineSeparator()
                                + contact.getCompetitorThree()
                                + System.lineSeparator() + System.lineSeparator()
                                + "Just trying to make sure you're the right person to send this to..."
                                + System.lineSeparator() + System.lineSeparator()
                                + "If you are, please reply to this email and I'll send it right over. If not, just let me know."
                                + System.lineSeparator() + System.lineSeparator()
                                + "Have a great day!"
                                + System.lineSeparator() + System.lineSeparator()
                                + "Thanks again,"
                                + System.lineSeparator() + System.lineSeparator()
                                + "Chris" + System.lineSeparator()
                                + "chris@dbagency.online"
                );

                message.setText(text);
                emailSender.send(message);

                contact.setLastSendDate(new Date());
                contact.setAutomationStep(2);

                contactService.update(contact);

                result.add(contact);
            }
        }
        return result;
    }

    public List<Contact> sendThirdEmail() {
        List<Contact> allContactsToThirdEmail = contactService.findActiveByAutomationStep(2);
        List<Contact> result = new ArrayList<>();

        for (Contact contact : allContactsToThirdEmail) {

            if (diffDate(contact) > 2.5 * dayLongValue) {

                SimpleMailMessage message = new SimpleMailMessage();
                message.setFrom("chris@dbagency.online");
                message.setTo(contact.getEmail());
                message.setSubject("Quick question about " + contact.getCompanyName());

                String text = new String(
                        "Why this URL: " + contact.getCompetitorThree() + " is beating yours."
                                + System.lineSeparator() + System.lineSeparator()
                                + "Just in case you haven't seen my other two emails, I put together a personalized "
                                + "video for you explaining exactly why your top three competitors are out-ranking you in Google."
                                + System.lineSeparator() + System.lineSeparator()
                                + "And because they're out-ranking you, you're missing out on multiple calls a day."
                                + System.lineSeparator() + System.lineSeparator()
                                + "These are the websites stealing your traffic:"
                                + System.lineSeparator()
                                + contact.getCompetitorOne()
                                + System.lineSeparator()
                                + contact.getCompetitorTwo()
                                + System.lineSeparator()
                                + contact.getCompetitorThree()
                                + System.lineSeparator() + System.lineSeparator()
                                + "The video is right here. I just need to make sure I don't send it to the wrong person."
                                + System.lineSeparator() + System.lineSeparator()
                                + "Let me know if you are the right person."
                                + System.lineSeparator() + System.lineSeparator()
                                + "Thanks,"
                                + System.lineSeparator() + System.lineSeparator()
                                + "Chris" + System.lineSeparator()
                                + "chris@dbagency.online"
                );

                message.setText(text);
                emailSender.send(message);

                contact.setLastSendDate(new Date());
                contact.setAutomationStep(3);

                contactService.update(contact);

                result.add(contact);
            }
        }
        return result;
    }

    private Long diffDate(Contact contact) {
        Long result = new Date().getTime() - contact.getLastSendDate().getTime();
        return result;
    }
}
