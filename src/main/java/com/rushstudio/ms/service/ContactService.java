package com.rushstudio.ms.service;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import com.rushstudio.ms.model.Contact;
import com.rushstudio.ms.model.ScrapeData;
import com.rushstudio.ms.model.ScrapeObject;
import com.rushstudio.ms.repository.ContactRepository;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Service
public class ContactService {

    private ContactRepository contactRepository;

    public ContactService(ContactRepository contactRepository) {
        this.contactRepository = contactRepository;
    }

    public List<Contact> readCsv(ScrapeData data) throws IOException {
        List<Contact> result = new ArrayList<>();

        String filePath = data.getFilePath();
        Path path = Paths.get(filePath);

        try (BufferedReader br = Files.newBufferedReader(path, StandardCharsets.UTF_8)) {

            HeaderColumnNameMappingStrategy<ScrapeObject> strategy
                    = new HeaderColumnNameMappingStrategy<>();
            strategy.setType(ScrapeObject.class);

            CsvToBean<ScrapeObject> csvToBean = new CsvToBeanBuilder<ScrapeObject>(br)
                    .withMappingStrategy(strategy)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build();

            List<ScrapeObject> leads = csvToBean.parse();

            for (ScrapeObject so : leads) {
                Contact contact = new Contact();
                contact.setCompanyName(so.getCompany());
                contact.setAddress(so.getAddress());
                contact.setCity(so.getCity());
                contact.setRegion(so.getRegion());
                contact.setZip(so.getZip());
                contact.setCountry(so.getCountry());
                contact.setPhone(so.getPhone());
                contact.setEmail(so.getEmail());
                contact.setWebsite(so.getWebsite());
                contact.setCategory(so.getCategory());
                contact.setCompetitorOne(data.getCompetitorOne());
                contact.setCompetitorTwo(data.getCompetitorTwo());
                contact.setCompetitorThree(data.getCompetitorThree());
                result.add(contact);
            }

            contactRepository.saveAll(result);

        } catch (IOException e) {

            e.printStackTrace();

        }

        return result;
    }

    public List<Contact> findActiveByAutomationStep(Integer step) {
        return contactRepository.findActiveByAutomationStep(step);
    }

    public Contact update(Contact contact) {
        return contactRepository.save(contact);
    }
}
